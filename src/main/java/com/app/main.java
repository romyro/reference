package com.app;

import com.opencsv.CSVWriter;
import com.qoppa.pdf.PDFException;
import com.qoppa.pdfText.PDFText;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class main {
    public static void main(String[] args) throws PDFException, IOException {
        final File carpeta = new File(args[0]);
        listarFicheros(carpeta);
    }

    private static void listarFicheros(final File carpeta) throws PDFException, IOException {
        String archivo;
        String direccion = String.valueOf(carpeta);
        String ruta = "C:\\Users\\user\\Desktop\\resultado\\ ";
        CSVWriter writer = new CSVWriter(new FileWriter("file.csv"));
        String[] entries = "numero#nombre#autor".split("#");

        ArrayList<String> buffer = new ArrayList<>(0);
        writer.writeNext(entries);
        for (final File fichero : carpeta.listFiles()) {
            if (fichero.isDirectory()) {
                listarFicheros(fichero);
            } else {
                archivo = direccion + "/" + fichero.getName();
                if (!(archivo == "null")) {
                    PDFText pdfText = new PDFText(archivo, null);
                    System.out.print("\n{\tName File: ");
                    buffer.add(pdfText.getFileName());
                    String[] test = new String[buffer.size()];
                    buffer.toArray(test);
                    writer.writeNext(test);
                    System.out.print(pdfText.getFileName()+",\t");
                    System.out.print(" \nTitle: ");
                    System.out.print(pdfText.getDocumentInfo().getTitle()+",\t");
                    System.out.print(" \nAuthor: ");
                    System.out.print(pdfText.getDocumentInfo().getAuthor()+",\t");
                    System.out.print(" \nCreator: ");
                    System.out.print(pdfText.getDocumentInfo().getCreator()+",\t");
                    System.out.print(" \nCreationDate: ");
                    System.out.print(pdfText.getDocumentInfo().getCreationDate()+",\t}\n");
                }
            }
        }
        writer.close();//CERRAMOS EL ESCRITOR
    }
}
